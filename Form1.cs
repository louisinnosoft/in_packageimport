﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Innosoft;
using System.IO;
using Aras.IOM;
using System.Xml;

namespace in_packageimport1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Innosoft.InnovatorHelper InnH;
        bool IsLoginOk;
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                if (Innosoft.InnUtility.IsProcessExist(System.Diagnostics.Process.GetCurrentProcess().ProcessName))
                    //Joe修改
                    //Environment.Exit(0);
                    FormClose();

                if (!Innosoft.InnUtility.IsAllowedWinUser())
                {
                    MessageBox.Show("目前登入者為:" + Innosoft.InnUtility.GetCurrentUser() + ",本程式僅允許Windows 使用者:" + Innosoft.InnUtility.GetAllowedWinUser() + "執行");
                    //Joe修改
                    //Environment.Exit(0);
                    FormClose();
                }


                txtUrl.Text = InnUtility.GetSettingValue("InnosoftCore", "url", "");
                txtDb.Text = InnUtility.GetSettingValue("InnosoftCore", "db", "");
                txtLoginId.Text = InnUtility.GetSettingValue("InnosoftCore", "login_id", "");
                txtPwd.Text = InnUtility.GetSettingValue("InnosoftCore", "pwd", "");

                txtImportFolder.Text = InnUtility.GetSettingValue("PackageImport", "importfolder", "");

                this.Text += "-" + Application.ProductVersion;


            }
            catch (Exception ex)
            {
                Innosoft.InnUtility.AddLog(ex.Message, "Error", txtMessage);
                MessageBox.Show(ex.Message);
                //Joe修改
                //Environment.Exit(0);
                FormClose();
            }
        }

        public void FormClose()
        {
            if (InnH != null)
            {
                InnH.LogoutAndExit();
            }
            SaveConfig();
            Environment.Exit(0);
        }

        private void btnSelectImportFolder_Click(object sender, EventArgs e)
        {
            if (txtImportFolder.Text != "")
                folderBrowserDialog1.SelectedPath = txtImportFolder.Text;
            folderBrowserDialog1.ShowDialog();
            if (folderBrowserDialog1.SelectedPath == "")
                return;
            txtImportFolder.Text = folderBrowserDialog1.SelectedPath;
            



        }

        private void ShowDirInfo()
        {
            lvSteps.Items.Clear();
            //若有子階資料夾,則列出各資料夾中的數量
            if (!Directory.Exists(txtImportFolder.Text))
                return;
            var directories = Directory.GetDirectories(txtImportFolder.Text);

            if (directories.Length == 0)
            {
                directories = new string[1];
                directories[0] = txtImportFolder.Text;
            }
            foreach (var dir in directories)
            {
                DirectoryInfo d = new DirectoryInfo(dir);
                FileInfo[] subFiles = d.GetFiles();
                string[] row = { d.Name, subFiles.Length.ToString(), "0", d.FullName };

                ListViewItem subFolder = new ListViewItem(row);
                subFolder.Checked = true;
                if (lvSteps.Columns[0].Width < d.Name.Length * 8)
                    lvSteps.Columns[0].Width = d.Name.Length * 8;
                lvSteps.Items.Add(subFolder);
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormClose();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

            if (!IsLoginOk)
            {
                MessageBox.Show("請先執行登入測試");
                return;
            }
            SaveConfig(); 
            


            string LogPath = InnUtility.GetBaseFolder() + @"\logs\" + System.DateTime.Now.ToString("MMddHHmm");

            foreach(ListViewItem step in lvSteps.Items)
            {
                if (!step.Checked)
                    continue;
                step.Selected = true;
                txtMessage.Text = "";
                step.SubItems[2].Text = "0";
                
                DirectoryInfo d = new DirectoryInfo(step.SubItems[3].Text);
                FileInfo[] subFiles = d.GetFiles();
                progressBar1.Maximum = subFiles.Length;
                progressBar1.Value = 0;
                foreach (FileInfo f in subFiles)
                {
                    Application.DoEvents();
                    string state = "Success";
                    InnUtility.AddLog("process:" + f.Name, "info", txtMessage);
                    progressBar1.Value++;
                    step.SubItems[1].Text = progressBar1.Value.ToString() + "/" + progressBar1.Maximum.ToString();
                    string eachFileLog = "";
                    eachFileLog = f.Name + Environment.NewLine;
                    switch (f.Extension)
                    {
                        case ".aml":
                            string Context = File.ReadAllText(f.FullName, Encoding.UTF8);
                            XmlDocument xmlDoc = new XmlDocument();
                            try
                            {                                
                                xmlDoc.LoadXml(Context);
                            }
                            catch(Exception ex)
                            {
                                eachFileLog += "非XML格式";
                                state = "Fail";                                
                                goto CloseFileLog;
                            }

                            if (xmlDoc.DocumentElement.Name != "Item" && xmlDoc.DocumentElement.Name != "AML")
                            {
                                eachFileLog += "非AML 或 Item 格式";
                                state = "Fail";
                                goto CloseFileLog;

                            }
                            else if (xmlDoc.DocumentElement.Name == "Item")
                            {
                                Context = "<AML>" + Context + "</AML>";
                            }

                            try
                            {
                                Item itmR = InnH.getInn().applyAML(Context);
                                if(itmR.isError())
                                {
                                    eachFileLog += itmR.getErrorString();
                                    state = "Fail";
                                    goto CloseFileLog;
                                }
                                else
                                {
                                    string ApplyResult = "";
                                   
                                    if(itmR.getItemCount()>1)
                                    {
                                        for(int i=0;i<itmR.getItemCount();i++)
                                        {
                                            Item EachR = itmR.getItemByIndex(i);
                                            ApplyResult += "(type='" + EachR.getType() + "',id='" + EachR.getID() + "',keyed_name='" + EachR.getProperty("keyed_name", "") + "),";
                                        }
                                    }

                                    if(itmR.getItemCount()==1)
                                    {
                                        ApplyResult = "type='" + itmR.getType() + "',id='" + itmR.getID() + "',keyed_name='" + itmR.getProperty("keyed_name", "");
                                    }
                                    else
                                    {
                                        //如果是delete之類只回傳字串或沒內容
                                        ApplyResult = itmR.getResult();

                                    }
                                    eachFileLog += "成功," + ApplyResult;
                                    state = "Success";
                                    goto CloseFileLog;
                                }
                            }
                            catch (Exception ex)
                            {

                                string errMsg = "";
                                if (ex.GetType().ToString() == "Aras.Server.Core.InnovatorServerException")
                                {
                                    Aras.Server.Core.InnovatorServerException arasEx = (Aras.Server.Core.InnovatorServerException)ex;
                                    XmlDocument errDom = new XmlDocument();
                                    arasEx.ToSoapFault(errDom); // Retreive inner exception
                                                           // Grab the value of the <faultstring> tags
                                    errMsg = errDom.GetElementsByTagName("faultstring")[0].InnerXml;
                                }
                                else
                                {
                                    errMsg = ex.Message;
                                }

                                eachFileLog += errMsg;
                                state = "Fail";
                                goto CloseFileLog;
                            }
                            break;
                        case ".sql":
                            string SqlContext = File.ReadAllText(f.FullName, Encoding.UTF8);
                            try
                            {
                                Item itmR = InnH.getInn().applySQL(SqlContext);
                                if (itmR.isError())
                                {
                                    eachFileLog += itmR.getErrorString();
                                    state = "Fail";
                                    goto CloseFileLog;
                                }
                                else
                                {
                                    eachFileLog += "成功";
                                    state = "Success";
                                    goto CloseFileLog;
                                }
                            }
                            catch (Exception ex)
                            {

                                string errMsg = "";
                                if (ex.GetType().ToString() == "Aras.Server.Core.InnovatorServerException")
                                {
                                    Aras.Server.Core.InnovatorServerException arasEx = (Aras.Server.Core.InnovatorServerException)ex;
                                    XmlDocument errDom = new XmlDocument();
                                    arasEx.ToSoapFault(errDom); // Retreive inner exception
                                                                // Grab the value of the <faultstring> tags
                                    errMsg = errDom.GetElementsByTagName("faultstring")[0].InnerXml;
                                }
                                else
                                {
                                    errMsg = ex.Message;
                                }

                                eachFileLog += errMsg;
                                state = "Fail";
                                goto CloseFileLog;
                            }
                            break;
                        default:
                            eachFileLog += "失敗,副檔名為:" + f.Extension + ",無法執行";
                            state = "Fail";
                            goto CloseFileLog;
                            break;
                    }

                    CloseFileLog:
                    eachFileLog = "[" + state + "]" + eachFileLog;
                    if(state=="Fail")
                    {
                        int errcount = int.Parse(step.SubItems[2].Text);
                        errcount++;
                        step.SubItems[2].Text = errcount.ToString();
                    }                   
                    eachFileLog = "-----------------------------------------------" + Environment.NewLine  + eachFileLog + Environment.NewLine + "-----------------------------------------------" + Environment.NewLine;
                    InnUtility.WriteTxtFile(LogPath, step.SubItems[0].Text + ".txt",eachFileLog, true, true);
                    InnUtility.AddLog(eachFileLog, "info", txtMessage);
                }
            }
           
        }

        private bool InitInnovator()
        {
            string strMessage = "";
            if (InnH != null)
                return true;
            try
            {

                InnH = new InnovatorHelper(out strMessage);
                if (InnH == null)
                {
                    //MessageBox.Show(strMessage);
                    InnUtility.AddLog(strMessage, "Debug", txtMessage);
                    return false;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                InnUtility.AddLog(strMessage, "Fatal", txtMessage);
                return false;
            }
            InnUtility.AddLog("登入成功", "Info", txtMessage);
            //HttpServerConnection httpServerConnection = InnH.getInn().getConnection() as HttpServerConnection;
            //httpServerConnection.Timeout = 1000000000;


            return true;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {

            SaveConfig();

            picNG.Visible = false;
            picOk.Visible = false;
            string strMessage = "";
            try
            {
                InnH = new InnovatorHelper(txtUrl.Text, txtLoginId.Text, txtPwd.Text, txtDb.Text, out strMessage);
                if (strMessage != "ok")
                {
                    MessageBox.Show(strMessage);
                    picNG.Visible = true;
                }
                else
                {
                    picOk.Visible = true;
                    IsLoginOk = true;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                picNG.Visible = true;
            }

        }

        private void btnClearMessage_Click(object sender, EventArgs e)
        {
            txtMessage.Text = "";
        }

        private void SaveConfig()
        {
            InnUtility.SetSettingValue("InnosoftCore", "url", txtUrl.Text);
            InnUtility.SetSettingValue("InnosoftCore", "db", txtDb.Text);
            InnUtility.SetSettingValue("InnosoftCore", "login_id", txtLoginId.Text);
            InnUtility.SetSettingValue("InnosoftCore", "pwd", txtPwd.Text);

            InnUtility.SetSettingValue("PackageImport", "importfolder", txtImportFolder.Text);



        }

        private void txtImportFolder_TextChanged(object sender, EventArgs e)
        {
            ShowDirInfo();
        }
    }
}
